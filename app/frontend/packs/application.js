/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

// references
// https://evilmartians.com/chronicles/evil-front-part-1
// https://medium.com/@coorasse/goodbye-sprockets-welcome-webpacker-3-0-ff877fb8fa79

import ActionCable from 'actioncable'
import * as ActiveStorage from 'activestorage'
import Rails from 'rails-ujs';
import Turbolinks from 'turbolinks';
import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"

import "./application.css";
import "bulma/bulma.sass";
import "normalize.css/normalize.css"

ActiveStorage.start();
Rails.start();
Turbolinks.start();
// let cable = ActionCable.createConsumer();

const application = Application.start()
const context = require.context("controllers", true, /\.js$/)
application.load(definitionsFromContext(context))

console.log('Hello World from Webpacker')

function railsGet(url) {
  return new Promise((resolve, reject) => {
    // Rails.ajax comes with csrf token request header
    Rails.ajax({
      url,
      type: "GET",
      success: data => {
        resolve(data);
      },
      error: (_jqXHR, _textStatus, errorThrown) => {
        reject(errorThrown);
      }
    });
  });
}

railsGet("/todos.json").then((data) => {
  data.forEach((d) => {
    console.log("******Todo item #", d.id);
    console.log("content:", d.content);
    console.log("complete:", d.complete);
    console.log("***************");
  });
}, (err) => console.log(err));;
