import { Controller } from "stimulus"

export default class extends Controller {
  static targets = ["body"];

  open() {
    this.bodyTarget.classList.add("is-active");
    // console.log(a === this.bodyTarget); // -> true
  }

  close() {
    this.bodyTarget.classList.remove("is-active");
  }

  get todosCtrl() {
    // find other stimulus controller by name
    let todosElement = document.getElementById("todos");
    return this.application.getControllerForElementAndIdentifier(todosElement, "todos");
  }
}
