import Rails from 'rails-ujs';
import { Controller } from "stimulus"

export default class extends Controller {
  static targets = ["item"];

  count() {
    console.log(`Hello, total todos: ${this.itemCount}!`);
    console.log(this.element);
    // this.itemTargets[0].remove(); // also remove DOM element
  }

  openModal() {
    this.modalCtrl.open(); // invoke method on modal controller
  }

  get itemCount() {
    return this.itemTargets.length;
  }

  get modalCtrl() {
    let modalElement = document.getElementById("modal");
    return this.application.getControllerForElementAndIdentifier(modalElement, "modal");
  }
}
